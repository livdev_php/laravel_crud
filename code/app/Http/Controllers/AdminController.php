<?php

namespace App\Http\Controllers;

use App\Http\Service\AdminService;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Главная страница
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home(){
        $data['menus']=AdminService::listMenu();
        $data['nameMenu']=AdminService::nameMenu();
        return view('admin.layouts.app',$data);
    }

    public function crudList($name){



        $dataConfig=AdminService::getFile($name);
        $model_name = $dataConfig['table'];
        $model = app("App\\Models\\".$model_name);

        $data['datas']=$model::all();
        $data['menus']=AdminService::listMenu();
        $data['nameMenu']=AdminService::nameMenu();
        $data['dataConfig']=$dataConfig;

        return view('admin.crudlist',$data);
    }
}
