<?php
namespace App\Http\Service;

class AdminService
{
    static function listMenu(){
        $value = config('admin.menu.menus');
        return $value;
    }

    static function nameMenu(){
        $value = config('admin.menu.nameAdmin');
        return $value;
    }

    static function getFile($name){
        $value = config('admin.'.$name);
        return $value;
    }
}
