<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Меню админки которые будут грузится
    |--------------------------------------------------------------------------
    |
    | Меню админки .
    | Пишется группа а потом файл в папке  admin  и название в админке
    */
    // Название сайта
    'nameAdmin' => 'My app',
    // Меню
    'menus' => [
        'users' => 'Table users',
        'posts' => 'Table posts',
    ]
];
