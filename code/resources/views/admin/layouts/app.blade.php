<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <title>Adminka</title>
    </head>
    <body>

        <div class="wrapperhead">
            {{$nameMenu}}
        </div>
        <div class="wrapperbody">
            <div class="leftitem">
                <lu>
                    @foreach($menus as $key=>$menu)
                        <li><a href="{{ url('/admincrud/'.$key.'/list') }}">{{$menu}}</a> </li>
                    @endforeach
                </lu>

            </div>
            <div class="rightitem">
                @yield('content', 'Default content')
            </div>
        </div>

    </body>
</html>
