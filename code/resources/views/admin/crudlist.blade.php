<!-- resources/views/child.blade.php -->

@extends('admin.layouts.app')


@section('content')
    <h1><b>{{$dataConfig['title']}}</b></h1>
    <div class="croski">
        <span>Главная</span>/
        <span>{{$dataConfig['title']}}</span>
    </div>
    <div class="wrapper_tablhead">
        <div class="add">Добавить</div>
    </div>
    <div class="wrapper_tables">
        <table>
            <tr>
                @foreach($dataConfig['columns'] as $tdName)
                    <td>{{$tdName}}</td>
                @endforeach
            </tr>

        </table>
    </div>
@endsection
